import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:progress_hud/progress_hud.dart';


abstract class OnDropdownEvent {
  void onDropdown(
      Object object);
}

abstract class OnComboBoxEvent {
  void onComboBoxEvent(
      Object object);
}

abstract class ConvertToList{

  List<List> convertToList(List arrayValues);
}

class UIHelper {
  static const bool isFill = true;

  static const double margin = 8;

  static const double marginWidth = 5;

  static double navigateIconSize = 35;

  static Color addFormContainerBodyColor = Color.fromRGBO(240, 240, 240, 9);

  static Color borderColor = Color.fromRGBO(192, 192, 192, 9);

  static bool isLabelCenter = true;

  OnDropdownEvent _ondropdown;

  OnComboBoxEvent _onComboBoxEvent;

  UIHelper(this._ondropdown);

  UIHelper.fromUIHelper(this._ondropdown, this._onComboBoxEvent);

  static TextStyle getTextStyleForLabel() {
    return new TextStyle(
      color: Colors.cyan,
      decoration: TextDecoration.none,
      fontSize: 16,
      fontFamily: 'OpenSans',
      fontWeight: FontWeight.w600,
    );
  }


  static InputDecoration getInputDirectionDisable(String labelName,
      bool isRequired) {
    return new InputDecoration(
      filled: true,
      errorText: isRequired ? 'Field Can\'t Be Empty' : null,
      labelStyle: isRequired
          ? TextStyle(color: Colors.cyan)
          : TextStyle(color: Colors.black87),
      labelText: labelName,
      fillColor: Colors.white,
      enabled: false,
      enabledBorder: isRequired
          ? UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.cyan, width: 0.5),
      )
          : UnderlineInputBorder(
          borderSide: BorderSide(
              color: Color.fromRGBO(192, 192, 192, 9), width: 0.5)),
    );
  }

  static InputDecoration getInputDirectionForRequired(String labelName) {
    return new InputDecoration(
      filled: true,
      labelStyle: TextStyle(color: Colors.cyan),
      labelText: labelName,
      fillColor: Colors.white,
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.cyan, width: 0.5),
      ),
    );
  }

  static InputDecoration getInputDirectionForDate(String labelName) {
    return new InputDecoration(
      suffixIcon: Icon(Icons.date_range, size: 20, color: Colors.cyan),
      filled: true,
      labelText: labelName,
      labelStyle: TextStyle(color: Colors.black87),
      fillColor: Colors.white,
      enabledBorder: UnderlineInputBorder(
        borderSide:
        BorderSide(color: Color.fromRGBO(192, 192, 192, 9), width: 0.5),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.cyan),
      ),
    );
  }

  static InputDecoration getInputDirectionForDateRequired(String labelName,
      bool isRequired, String errorText) {
    return new InputDecoration(
      suffixIcon: Icon(Icons.date_range, size: 20, color: Colors.cyan),
      filled: true,
      errorText: isRequired ? errorText : null,
      labelText: labelName,
      fillColor: Colors.white,
      labelStyle: TextStyle(color: Colors.black87),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: borderColor, width: 0.5),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.cyan),
      ),
    );
  }

  static TextStyle getTextStyleFormField() {
    return new TextStyle(
      fontSize: 16,
      fontFamily: 'OpenSans',
    );
  }

  static showToast(String msg) {
    return Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.cyan,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static void requestLoader(BuildContext _ctx) {
    ProgressHUD _progressHUD = new ProgressHUD(
      backgroundColor: Colors.transparent,
      color: Colors.white,
      containerColor: Colors.cyan,
      borderRadius: 5.0,
      text: 'Processing...',
    );
    showDialog(
      context: _ctx,
      builder: (BuildContext context) {
        // return object of type Dialog
        return _progressHUD;
      },
    );
  }

  static Expanded dataWidget(BuildContext context, DateTime selectedDate,
      TextEditingController _dateController, String labelName) {
    return new Expanded(
        child: GestureDetector(
            onTap: () {
              selectDate(context, selectedDate, _dateController);
              print('date ');
            },
            child: AbsorbPointer(
                child: new TextFormField(
                  //initialValue: vvv(),
                    controller: _dateController,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Please Select Date';
                      }
                    },
                    style: UIHelper.getTextStyleFormField(),
                    decoration:
                    UIHelper.getInputDirectionForDate(labelName)))));
  }


  static Widget DateWidget(BuildContext context, var value, bloc) {


  }

  static Widget divWidget(String labelName) {
    return new Container(
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          FlatButton(
            child: Text(
              labelName,
              style: TextStyle(
                color: Colors.cyan,
                decoration: TextDecoration.none,
                fontSize: 15,
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
      height: 50.0,
      margin: EdgeInsets.only(bottom: 10.0, top: 1.0),
      decoration: new BoxDecoration(
        shape: BoxShape.rectangle,
        color: Color.fromRGBO(242, 242, 242, 1),
      ),
      padding: EdgeInsets.all(0.0),
    );
  }

  static String getDate(String value) {
    String result;
    if (value.contains('span')) {
      result = between(value, '>', '</span>');
    } else {
      result = value;
    }
    return result;
  }

  static String between(String value, String a, String b) {
    // Return a substring between the two strings.
    int posA = value.indexOf(a);
    if (posA == -1) {
      return "";
    }
    int posB = value.lastIndexOf(b);
    if (posB == -1) {
      return "";
    }
    int adjustedPosA = posA + a.length;
    if (adjustedPosA >= posB) {
      return "";
    }
    return value.substring(adjustedPosA, posB);
  }

  static Future<String> selectDate(BuildContext context, DateTime selectedDate,
      TextEditingController _dateController) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      //String formattedDate =
      selectedDate = picked;
    _dateController.text = DateFormat('yyyy-MM-dd').format(picked);

    return DateFormat('yyyy-MM-dd').format(picked);
  }


  static Future<bool> isInterNetConnectionAvailable() async {
    bool connection = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connection = true;
        print('connected');
      }
    } on SocketException catch (_) {
      connection = false;
      print('not connected');
      Fluttertoast.showToast(
          msg: "opps! No internet Connection",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    return connection;
  }
}