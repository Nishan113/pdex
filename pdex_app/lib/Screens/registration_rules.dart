import 'package:flutter/material.dart';
import 'package:pdex_app/Screens/password_set.dart';
import 'package:pdex_app/Screens/registration_for_delivery.dart';
import 'package:pdex_app/Screens/settings.dart';


class RegistrationRules extends StatefulWidget {
  @override
  _RegistrationRulesState createState() => _RegistrationRulesState();
}

class _RegistrationRulesState extends State<RegistrationRules> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => PasswordSet()));
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.white,
          ),
        ),
        title: Text('ডেলিভারি রেজিস্ট্রেশন শর্তাবলী'),
      ),
      body: Column(
        children: [
          Container(
            child: Text(
              'ডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলী'
            ),
          ),
           Align(
            alignment: Alignment.bottomCenter,
            child: RaisedButton(
              onPressed: () {
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (BuildContext context) => RegistrationForDelivery()));
              },
              child: const Text('I AGREE', style: TextStyle(fontSize: 20)),
              color: Colors.blue,
              textColor: Colors.white,
              elevation: 5,
            ),
          ),


        ],
      ),
    );
  }
}
