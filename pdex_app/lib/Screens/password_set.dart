import 'package:flutter/material.dart';
import 'package:pdex_app/Screens/loginScreen.dart';
import 'package:pdex_app/Screens/pickup_information.dart';
import 'package:pdex_app/Screens/registration_for_delivery.dart';
import 'package:pdex_app/Screens/registration_rules.dart';

class PasswordSet extends StatefulWidget {
  @override
  _PasswordSetState createState() => _PasswordSetState();
}

class _PasswordSetState extends State<PasswordSet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(

        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => PickUpInformation()));
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.white,
          ),
        ),
        title: new Center(child: Text(
          'পাসওয়ার্ড তৈরী করুন',
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 100,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child: Image.asset('assets/images/holding-phone.png')),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'পাসওয়ার্ড',
                    hintText: 'পাসওয়ার্ড'),
              ),
            ),
            SizedBox(height: 25,),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'পাসওয়ার্ড নিশ্চিত করুন',
                    hintText: 'পাসওয়ার্ড নিশ্চিত করুন'),
              ),
            ),


            SizedBox(height: 25,),

            Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Align(
                        alignment: Alignment.center,
                        child: new ButtonTheme(
                            minWidth: 300.0,
                            height: 50.0,
                            child: new OutlineButton(
                              onPressed: () {
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => RegistrationRules()));
                              },
                              child: Text("NEXT",
                                  style:
                                  TextStyle(color: Colors.green, fontFamily: 'OpenSans')),
                              borderSide: BorderSide(color: Colors.black),
                              shape: StadiumBorder(
                              ),
                            ))),
                  ],
                ),
              ),

            ),

          ],
        ),
      ),

    );
  }
}