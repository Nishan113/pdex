import 'package:flutter/material.dart';
import 'package:pdex_app/Screens/delivery_registration.dart';
import 'package:pdex_app/Screens/registration_rules.dart';
import 'package:pdex_app/Screens/settings.dart';

class RegistrationForDelivery extends StatefulWidget {
  @override
  _RegistrationForDeliveryState createState() =>
      _RegistrationForDeliveryState();
}

class _RegistrationForDeliveryState extends State<RegistrationForDelivery> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white60,
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => RegistrationRules()));
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.white,
          ),
        ),
        title: Text('দোকানের নাম'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          Settings()));
            },
          ),
        ],
      ),

      // TODO: Add a grid view (102)
      /*body: Container(
          alignment: Alignment.center,
          child: ListTile(
            title: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                children: [
                  Container(
                height: 70,
                 width: 150,
                      child: RaisedButton(
                        onPressed: () {},
                        child: Text(" পার্সেল \n আপডেট \n দেখুন"),
                        color: Colors.white,
                        textColor: Colors.black,
                      )
            )
              ],
            )
                   ),
                SizedBox(
                  width: 20.0,
                ),
                Expanded(
                    child: Column(
                      children: [
                        Container(
                            height: 70,
                            width: 150,
                            child: RaisedButton(
                              onPressed: () {},
                              child: Text(" পার্সেল \n ডেলিভারি \n করুন"),
                              color: Colors.white,
                              textColor: Colors.black,
                            )
                        )
                      ],
                    )
                ),

                Expanded(child: Column(children: [
                  Container(height: 120.0, color: Colors.yellow),
                  Container(height: 100.0, color: Colors.cyan),
                ]))
              ],
            ),
          ),

      ),*/
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            new Row(
              children: [
                SizedBox(width: 15.0,),
                new Expanded(child:
                        FlatButton(
                        onPressed: () => {},
                        color: Colors.white,
                        padding: const EdgeInsets.all(8.0),
                        child: Column( // Replace with a Row for horizontal icon + text
                          children: <Widget>[
                            Icon(Icons.add),
                            Text("পার্সেল \n ডেলিভারি \n করুন  ")
                          ],
                        ),),
                ),
                SizedBox(width: 15.0,),
                new Expanded(child:
                        FlatButton(
                        onPressed: () => {},
                        color: Colors.white,
                        padding: const EdgeInsets.all(8.0),
                        child: Column( // Replace with a Row for horizontal icon + text
                          children: <Widget>[
                            Icon(Icons.add),
                            Text("পার্সেল \n আপডেট \n দেখুন   ")
                          ],
                        ),),

                ),
                SizedBox(width: 15.0,),
              ],
            ),

            SizedBox(height: 30.0,),
            new Row(
              children: [
                SizedBox(width: 15.0,),
                new Expanded(child:
                FlatButton(
                  onPressed: () => {},
                  color: Colors.white,
                  padding: const EdgeInsets.all(8.0),
                  child: Column( // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(Icons.add),
                      Text("পার্সেল \n ডেলিভারি \n করুন  ")
                    ],
                  ),),
                ),
                SizedBox(width: 15.0,),
                new Expanded(child:
                FlatButton(
                  onPressed: () => {},
                  color: Colors.white,
                  padding: const EdgeInsets.all(8.0),
                  child: Column( // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(Icons.add),
                      Text("পেমেন্ট \n আপডেট \n দেখুন  ")
                    ],
                  ),),

                ),
                SizedBox(width: 15.0,),
              ],
            ),
          ],

        ),
      ),

      /*Container(
        */ /*child: Row(
          children: [
        Card(
          clipBehavior: Clip.antiAlias,
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AspectRatio(
                aspectRatio: 18.0 / 11.0,
                child: Image.asset('assets/images/diamond.png'),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Title'),
                    SizedBox(height: 8.0),
                    Text('Secondary Text'),
                  ],
                ),
              ),
            ],
          ),
        ),


        Card(
          clipBehavior: Clip.antiAlias,
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AspectRatio(
                aspectRatio: 18.0 / 11.0,
                child: Image.asset('assets/images/diamond.png'),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Title'),
                    SizedBox(height: 8.0),
                    Text('Secondary Text'),
                  ],
                ),
              ),
            ],
          ),
        ),



        Card(
          clipBehavior: Clip.antiAlias,
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AspectRatio(
                aspectRatio: 18.0 / 11.0,
                child: Image.asset('assets/images/diamond.png'),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Title'),
                    SizedBox(height: 8.0),
                    Text('Secondary Text'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
        ),*/ /*
    ),*/
    );

    // body:Center(
    //   child: Row(
    //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //     //mainAxisAlignment: MainAxisAlignment.center,
    //
    //     children: <Widget>[
    //       Container(
    //         width: 100.0,
    //         height: 130.0,
    //         child: FlatButton(
    //           onPressed: () => {},
    //           color: Colors.orange,
    //           padding: const EdgeInsets.all(8.0),
    //           child: Column( // Replace with a Row for horizontal icon + text
    //             children: <Widget>[
    //               Icon(Icons.add),
    //               Text("পার্সেল \n ডেলিভারি \n করুন  ")
    //             ],
    //           ),
    //         ),
    //       ),
    //       Container(
    //         width: 100.0,
    //         height: 130.0,
    //         child: FlatButton(
    //           onPressed: () => {},
    //           color: Colors.orange,
    //           padding: EdgeInsets.all(8.0),
    //           child: Column( // Replace with a Row for horizontal icon + text
    //             children: <Widget>[
    //               Icon(Icons.add),
    //               Text("পার্সেল \n আপডেট \n দেখুন ")
    //             ],
    //           ),
    //         ),
    //       ),
    //       Container(
    //         width: 100.0,
    //         height: 130.0,
    //         child: FlatButton(
    //           onPressed: () => {
    //
    //           },
    //           color: Colors.orange,
    //           padding: EdgeInsets.all(8.0),
    //           child: Column( // Replace with a Row for horizontal icon + text
    //             children: <Widget>[
    //               Icon(Icons.add),
    //               Text("পেমেন্ট \n আপডেট \n দেখুন ")
    //             ],
    //           ),
    //         ),
    //       ),
    //     ],
    //   ),
    // ),

    // Center(child: Body()),
  }
}

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: RaisedButton(
        onPressed: () {},
        child: const Text('Bottom Button!', style: TextStyle(fontSize: 20)),
        color: Colors.blue,
        textColor: Colors.white,
        elevation: 5,
      ),
    );
  }
}

/*
// TODO: Make a collection of cards (102)
List<Card> _buildGridCards(int count) {
  List<Card> cards = List.generate(
    count,
        (int index) => Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          AspectRatio(
            aspectRatio: 18.0 / 11.0,
            child: Image.asset('assets/images/diamond.png'),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Title'),
                SizedBox(height: 8.0),
                Text('Secondary Text'),
              ],
            ),
          ),
        ],
      ),
    ),
  );

  return cards;
}*/
