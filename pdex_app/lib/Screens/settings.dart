import 'package:flutter/material.dart';
import 'package:pdex_app/Screens/delivery_rules.dart';
import 'package:pdex_app/Screens/log_out.dart';
import 'package:pdex_app/Screens/loginScreen.dart';
import 'package:pdex_app/Screens/other_information.dart';
import 'package:pdex_app/Screens/payment_details.dart';
import 'package:pdex_app/Screens/pickup_information.dart';
import 'package:pdex_app/Screens/registration_for_delivery.dart';
import 'package:pdex_app/Screens/registration_rules.dart';


class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => RegistrationForDelivery()));
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.white,
          ),
        ),



      ),
      body: ListView(
        children: [
          ButtonTheme(
            minWidth: 200.0,
            height: 70.0,
            child:RaisedButton(
                elevation: 5,
                child: Text('পেমেন্ট ডিটেলস ', style: TextStyle(fontSize: 20),),
                color: Colors.white,
                textColor: Colors.black,
                onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => PaymentDetails()));
                }),
          ),

          ButtonTheme(
            minWidth: 200.0,
            height: 70.0,
            child:RaisedButton(
                elevation: 5,
                child: Text('পিকআপ ইনফরমেশন ', style: TextStyle(fontSize: 20),),
                color: Colors.white,
                textColor: Colors.black,
                onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => PickUpInformation()));
                }),
          ),

          ButtonTheme(
            minWidth: 200.0,
            height: 70.0,
            child:RaisedButton(
                elevation: 5,
                child: Text('অন্যান্য ইনফরমেশন', style: TextStyle(fontSize: 20),),
                color: Colors.white,
                textColor: Colors.black,
                onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => OthersInformation()));
                }),
          ),

          ButtonTheme(
            minWidth: 200.0,
            height: 70.0,
            child:RaisedButton(
                elevation: 5,
                child: Text('ডেলিভারি শর্তাবলী ', style: TextStyle(fontSize: 20),),
                color: Colors.white,
                textColor: Colors.black,
                onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DeliveryRules()));
                }),
          ),

          ButtonTheme(
            minWidth: 200.0,
            height: 70.0,
            child:RaisedButton(
                elevation: 5,
                child: Text('লাইভ চ্যাট ', style: TextStyle(fontSize: 20),),
                color: Colors.white,
                textColor: Colors.black,
                onPressed: (){
                  //Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => RegistrationRules()));
                }),
          ),

          ButtonTheme(
            minWidth: 200.0,
            height: 70.0,
            child:RaisedButton(
                elevation: 5,
                child: Text('লগআউট ', style: TextStyle(fontSize: 20),),
                color: Colors.white,
                textColor: Colors.black,
                onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
                }),
          ),

        ],
      ),
    );
  }
}
